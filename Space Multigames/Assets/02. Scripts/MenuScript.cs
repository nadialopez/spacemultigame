﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

    public GameObject spaceship;
    public float speedRotation = 30.0f;
    
    
    
	// Use this for initialization
	void Start () {

        spaceship = GameObject.Find("SpaceShip"); //Si queremos encontrar un objeto por codigo agregar un tag al objeto
        //spaceship = GameObject.FindGameObjectsWithTag("SpaceShip"); //Si queremos encontrar un objeto por codigo agregar un tag al objeto
       // Debug.Log("MenuScript_Start");
	}
	
	// Update is called once per frame
	void Update () 
    {
          transform.Rotate(0.0f, Time.deltaTime * speedRotation, 0.0f);
          if (Input.GetKey(KeyCode.A))
          {
              spaceship.SetActive(false);
          }
	}

    void SayHi()
    {
        Debug.Log("Hi");
    }
}


